import React from "react"

import Style from "./style.module.css"

import Logo from "../../images/logo.svg"

const Header = () => {

	let items = ["Início", "Mercado", "Fundo Luggo", "Simulação", "Ativos", "Invista", "Ajuda"]

	let menu_items = items.map((item, index) => {
		return <li className={Style.menu_item} key={index}><a href="#" className={Style.menu_link}> {item} </a> </li>
	})

    function handleClick(e) {
		e.preventDefault();
		items.push('teste');
        console.log('O link foi clicado.');
    }

	return (
		<header className={Style.header}>
			<div className="container flex">
				<a href="//localhost:8000"> <Logo/> </a>
				<ul className={Style.menu}>
					<a href="" onClick={handleClick}>{menu_items}</a>
				</ul>
			</div>
		</header>
	)
}

export default Header