import React from "react"

import Style from "./style.module.css"

import Section from "../Section"
import TitleSection from "../TitleSection"
import TitleSubSection from "../TitleSubSection"

const SectionMercado = () => {
	return (
		<Section>
			<TitleSection> Mercado Imobiliário Residencial </TitleSection>

			<div className={Style.content}>
				<div>
					<TitleSubSection> Lançamentos </TitleSubSection>
					<p className={Style.description}> Desde 2017, o aquecimento econômico do setor imobiliário é cada vez mais significativo, consolidando um crescimento tão positivo que 2019 aparece como o momento ideal para investir no mercado imobiliário. </p>
					<p className={Style.box}> A Câmara Brasileira da Indústria da Construção registrou, somente no primeiro trimestre de 2019, um avanço de 4% no número de lançamentos de imóveis pelo Brasil, número que pode chegar a 15% no último registro que será auferido este ano. </p>
				</div>
				<div className={Style.right_content}>
					<TitleSubSection> Comercialização </TitleSubSection>
					<p className={Style.description}> Se nos atentarmos para os números referentes à comercialização de imóveis, os resultados são ainda mais animadores. </p>
					<p className={Style.box}> A CBID divulgou importantes dados no aspecto da vendas de imóveis. Segundo a Câmara, o avanço nas comercialização no primeiro trimestre atingiu 9,7% e também pode chegar aos 15% com a medição total do ano de 2019. </p>
				</div>
			</div>
		</Section>
	)
}

export default SectionMercado