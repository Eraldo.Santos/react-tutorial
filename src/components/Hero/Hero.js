import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import Style from "./style.module.css"

const Hero = () => {

	const data = useStaticQuery(graphql`
	    query {
	      inter: file(relativePath: { eq: "inter.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      plural: file(relativePath: { eq: "brasil-plural.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      genial: file(relativePath: { eq: "genial-investimentos.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      mrv: file(relativePath: { eq: "mrv.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      anbima: file(relativePath: { eq: "anbima.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      lobo: file(relativePath: { eq: "lobo-de-rizzo.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      },
	      luggo: file(relativePath: { eq: "luggo.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
	      }
	    }
	`)


	return (
		<section className={Style.hero}>
			<div className="container">
				<div className="flex">
					<div className={Style.content}>
						<span className={Style.label}> Material Publicitário </span>
						<h1 className={Style.title}> Luggo Fundo de Investimento Imobiliário </h1>
						<a className={Style.button} href="#"> Quero Investir </a>
					</div>
					<div className={Style.logos}>
						<Img fixed={data.inter.childImageSharp.fixed} />
						<div className={Style.row}>
							<Img className={Style.logo} fixed={data.plural.childImageSharp.fixed} />
							<Img className={Style.logo} fixed={data.genial.childImageSharp.fixed} />
							<Img className={Style.logo} fixed={data.mrv.childImageSharp.fixed} />
						</div>
						<div className={Style.row}>
							<Img className={Style.logo} fixed={data.anbima.childImageSharp.fixed} />
							<Img className={Style.logo} fixed={data.lobo.childImageSharp.fixed} />
							<Img className={Style.logo} fixed={data.luggo.childImageSharp.fixed} />
						</div>
					</div>
				</div>
				<p className={Style.disclaimer}> Não deixe de ler o prospecto e o regulamento do Fundo antes de aceitar a oferta, com atenção especial à seção “Fatores de risco”. </p>
			</div>
		</section>
	)
}

export default Hero