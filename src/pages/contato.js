import React from "react"
import { Link } from "gatsby"

import Layout from "../components/Layout"
import SEO from "../components/seo"
import Clock from "../components/Clock"
import SectionContato from "../components/SectionContato"
import FormContato from "../components/FormContato"

const Contato = () => (
  <Layout>
    <SEO title="Contato" />
    <Clock  />
    <FormContato />
    <SectionContato text="teste props pq o bagulho é doido" />
  </Layout>
)

export default Contato
