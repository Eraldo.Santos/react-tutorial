import React from "react"

import Style from "./style.module.css"

const Section = (props) => {

	let bgColor = "#ffffff"
	if(props.bgColor == "gray")
		bgColor = "#F5F5F5"

	return (
		<section style={{backgroundColor: bgColor}} className={Style.section}>
			<div className="container">
				{props.children}
			</div>
		</section>
	)
}

export default Section