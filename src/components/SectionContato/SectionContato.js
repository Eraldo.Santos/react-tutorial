import React from "react"

import Style from "./style.module.css"

import Section from "../Section"
import TitleSection from "../TitleSection"
import TitleSubSection from "../TitleSubSection"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"


const SectionContato = (props) => {

    const data = useStaticQuery(graphql`
	    query {
	      naruto: file(relativePath: { eq: "naruto.png" }) {
	        childImageSharp {
	          fixed {
	            ...GatsbyImageSharpFixed
	          }
	        }
          }
        }
    `)
          
	return (
		<Section>
			<TitleSection>Contato</TitleSection>

			<div className={Style.content}>
				<div className={Style.x}>
					<TitleSubSection> Eraldo </TitleSubSection>
                    <Img className={Style.circle} fixed={data.naruto.childImageSharp.fixed} />
					<p className={Style.description}> Programador mais brabo de Konoha, eu serei hokage do react esse é meu jeito ninja de ser. </p>
                    <div className={Style.box}>
                        <p> {props.text} </p>
                        <p> Email Eraldo.Santos_Neto@gmail.com. </p>
                    </div>
				</div>
                <div className={Style.x}>
					<TitleSubSection> Eraldo </TitleSubSection>
                    <Img className={Style.circle} fixed={data.naruto.childImageSharp.fixed} />
					<p className={Style.description}> Programador mais brabo de Konoha, eu serei hokage do react esse é meu jeito ninja de ser. </p>
                    <div className={Style.box}>
                        <p> Trabalho com todas as linguagens ninjas. </p>
                        <p> Email Eraldo.Santos_Neto@gmail.com. </p>
                    </div>
				</div>
                <div className={Style.x}>
					<TitleSubSection> Eraldo </TitleSubSection>
                    <Img className={Style.circle} fixed={data.naruto.childImageSharp.fixed} />
					<p className={Style.description}> Programador mais brabo de Konoha, eu serei hokage do react esse é meu jeito ninja de ser. </p>
                    <div className={Style.box}>
                        <p> Trabalho com todas as linguagens ninjas. </p>
                        <p> Email Eraldo.Santos_Neto@gmail.com. </p>
                    </div>
				</div>
			</div>
		</Section>
	);
}

export default SectionContato