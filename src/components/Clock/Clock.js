import React from "react"

import Style from "./style.module.css"

class Clock extends React.Component  {

  constructor(props) {
      super(props);
      this.state = {date: new Date()};
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }
  render() {
    return (
      <div className={Style.Clock_Container}>
        <h2 className={Style.Clock}>Agora são {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    )
  }
}

export default Clock