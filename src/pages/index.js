import React from "react"

import Layout from "../components/Layout"
import Hero from "../components/Hero"
import SectionMercado from "../components/SectionMercado"
import SectionOqueE from "../components/SectionOqueE"
import SectionVideo from "../components/SectionVideo"
import SectionVantagens from "../components/SectionVantagens"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Hero />
    <SectionMercado />
    <SectionOqueE />
    <SectionVideo />
    <SectionVantagens />
  </Layout>
)

export default IndexPage
