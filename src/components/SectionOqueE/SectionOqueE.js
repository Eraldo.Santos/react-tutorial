import React from "react"

import Style from "./style.module.css"

import Section from "../Section"
import TitleSubSection from "../TitleSubSection"
import HouseIcon from "../../images/house.svg"
import CertificateIcon from "../../images/certificate.svg"
import PercentIcon from "../../images/percent.svg"

const SectionOqueE = () => {
	return (
		<Section bgColor="gray">
			<div className={Style.title}>
				<TitleSubSection> O que é um Fundo de Investimento Imobiliário ? </TitleSubSection>
			</div>
			<div className={Style.content}>
				<div className={Style.box}>
					<HouseIcon />
					<p className={Style.description}> O Fundo de Investimento Imobiliário funciona como uma conjunção de recursos captados que são destinados à aplicação em empreendimentos imobiliários. </p>
				</div>
				<div className={Style.box}>
					<CertificateIcon />
					<p className={Style.description}> Além disso, os recursos também são aplicados em títulos e valores mobiliários de emissores cujas atividades estejam conectadas ao setor imobiliário ou, ainda, em títulos de dívidas privadas com um lastro imobiliário. </p>
				</div>
				<div>
					<PercentIcon />
					<p className={Style.description}> Os resultados obtidos a partir da renda de locações ou de rendimentos de aplicações mobiliárias são distribuídos proporcionalmente aos detentores de cotas do Fundo. </p>
				</div>
			</div>
		</Section>
	);
}

export default SectionOqueE