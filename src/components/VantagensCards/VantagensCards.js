import React from "react"

import Style from "./style.module.css"

import Liquidez from "../../images/liquidez.svg"
import Hand from "../../images/hand.svg"
import Pizza from "../../images/pizza.svg"
import Coin from "../../images/coin.svg"
import Eye from "../../images/eye.svg"

const VantagensCards = () => {
	return (
            <div className={Style.Vantagem}>
				<div className={Style.content}>
                    <div className={Style.titleImage}>
                        <Liquidez />
                        <h3 className={Style.titleVantagem}>Liquidez</h3>
                    </div>
					<p className={Style.text}>A cota de um Fundo Imobiliário é muito mais líquida, ou seja, tem um prazo de resgate menor e uma conversão do ativo em dinheiro mais rápida que a de um imóvel, por exemplo.</p>
				</div>
                <div className={Style.content}>
                    <div className={Style.titleImage}>
                        <Hand />
                        <h3 className={Style.titleVantagem}>Facilidade</h3>
                    </div>
					<p className={Style.text}>A simplicidade e facilidade de investir em um FII pode ser identificada na hora de vender suas cotas e de acompanhar a evolução do seu investimento. Além da vantajosa liquidez mensal, a negociação das cotas é caracterizada por ter pouca burocracia e menos trâmites legais que a maioria dos outros investimentos.</p>
				</div>
                <div className={Style.content}>
                    <div className={Style.titleImage}>
                        <Pizza />
                        <h3 className={Style.titleVantagem}>Diversiificação</h3>
                    </div>
					<p className={Style.text}>Enquanto a poupança e diversos outros tipos de investimentos tradicionais têm suas atuações restritas ao setor bancário e dependem prioritariamente da variação das taxas do governo, os segmentos contemplados por um Fundo Imobiliário são muito mais diversos, podendo estar vinculados a lajes corporativas, galpões industriais, galpões de logística, shoppings, universidades, lojas e supermercados, hospitais além de outros setores.</p>
				</div>
                <div className={Style.content}>
                    <div className={Style.titleImage}>
                        <Coin />
                        <h3 className={Style.titleVantagem}>Custos de Aquisição</h3>
                    </div>
					<p className={Style.text}>Se a aquisição de um imóvel requer o pagamento de comissão imobiliária, tributos e taxas de cartório, a transação de uma cota do Fundo Imobiliário tem apenas o custo de corretagem e, por isso, sai muito mais barata.</p>
				</div>
                <div className={Style.fiscal}>
                    <div className={Style.fiscalContainer}>
                        <div className={Style.titleImage}>
                            <Eye />
                            <h3 className={Style.titleVantagem}>Isenção Fiscal</h3>
                        </div>
                        <p className={Style.text}>Uma pessoa física que adquire cotas de um Fundo de Investimento Imobiliário têm seus rendimentos isentos do Imposto de Renda, uma vez que as seguintes condições sejam contempladas:</p>
                    </div>
                    <div className={Style.fiscalContainer}>
                        <p className={Style.text}><span className={Style.number}>1º</span> O cotista precisa ter no máximo 10% das cotas totais do Fundo e ter direito a receber um rendimento de também no máximo 10% do rendimento auferido pelo Fundo.</p>
                        <p className={Style.text}><span className={Style.number}>2º</span> A negociação das cotas deve ser exclusiva de bolsas de valores e do mercado de balcão organizado.</p>
                        <p className={Style.text}><span className={Style.number}>3º</span> As cotas do Fundo precisam ser distribuídas entre, no mínimo, 50 cotistas.</p>
                    </div>
                </div>
			</div>
        )
    }
    
export default VantagensCards