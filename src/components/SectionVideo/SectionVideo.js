import React from "react"

import Style from "./style.module.css"

import Section from "../Section"
import LuggoVideo from "../../videos/video.mp4"

const SectionVideo = () => {
	return (
		<Section>
			<div className={Style.content}>
				<p className={Style.description}> Sendo assim, quando você investe em um FII está consequentemente participando do crescimento do mercado imobiliário, que tende a ser cada vez maior. Além disso, a liquidez desse tipo de investimento é beneficiada pela negociação direta das cotas pela Bovespa. </p>
				<video className={Style.video} controls>
				    <source src={LuggoVideo} type="video/mp4" />
				</video>
			</div>
		</Section>
	)
}

export default SectionVideo