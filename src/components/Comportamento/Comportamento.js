import React from "react"

import Style from "./style.module.css"

import TitleSubSection from "../TitleSubSection"

const Comportamento = () => {
	return (

        <div className={Style.container}>
            <div>
                <TitleSubSection> Comportamento do mercado de Fundo Imobiliário </TitleSubSection>
                <p className={Style.text}>Os investimentos em FII (Fundo de Investimento Imobiliário) são ainda mais desejados em cenários que apresentam uma taxa básica de juros mais baixa. Nestas situações, o número de investidores e o volume de negociações geralmente se eleva muito.</p>
            </div>
            <div className={Style.graphContainer}>
                <p className={Style.description}>Com o aumento do volume de negociações, cresce também a liquidez no mercado de FII</p>
                <div>
                    <div className={Style.Graph}></div>
                    <p className={Style.textGraph}>O acumulado do volume anual de negociações em 2019 já ultrapassou o volume total de negociações em 2018</p>
                </div>
                <div>
                    <div className={Style.Graph}></div>
                    <p className={Style.textGraph}>O número de investidores em crescimento reflete o aumento do volume de negociações de FII.</p>
                </div>
                <p className={Style.fontGraph}>Fonte para ambos os gráficos: Boletim Imobiliário B3- Agosto 2019 e Bacen. 1. Relatório Focus 04/10/2019 *Acumulado até Agosto de 2019</p>
            </div>
        </div>
	);
}

export default Comportamento